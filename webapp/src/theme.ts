import { createTheme } from '@mantine/core';

export const theme = createTheme({
    fontFamily: 'BlinkMacSystemFont Segoe UI Monaco, sans-serif',
    fontFamilyMonospace: 'Monaco, Courier, monospace',
    headings: { fontFamily: 'Greycliff CF, sans-serif' },
});